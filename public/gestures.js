/**
 * Created by momomentum on 08.12.16.
 */
var hand;
var opts = {enableGestures: true};
var oldLows = 0;
var oldMids = 0;
var oldHighs = 0;
var lows = 0;
var mids = 0;
var highs = 0;
var rightTop = 0;
var rightMid = 0;
var rightBot = 0;
var oldRightTop = 0;
var oldRightMid = 0;
var oldRightBot = 0;
var knobSensitivity = 90;
var volume = 0;
var RightHandGrab = 0;
var leftHandFlatEffect = 0;
var rightHandFlatEffect = 0;
var jsonGestureLeft = "";
var jsonSelectorLeft = "";
var jsonValueLeft = "";
var jsonGestureRight = "";
var jsonSelectorRight = "";
var jsonValueRight = "";
var degree_high = 0;
var degree_mid = 0;
var degree_low = 0;
var degree_rightTop = 0;
var degree_rightMid = 0;
var degree_rightBot = 0;

var socket = io();



/**
 * Map input parameters to a specified output boundary
 * @param in_min
 * @param in_max
 * @param out_min
 * @param out_max
 * @returns {*}
 */
Number.prototype.map = function (in_min, in_max, out_min, out_max) {
    return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


/**
 * Change the low parameters and turn corresponding knob in html document
 */
function changeLows() {
    lows = hand.roll() * (180 / Math.PI) * (-1);
    if(Math.abs(oldLows - lows) < knobSensitivity) {
        oldLows = lows;
    } else {
        lows = oldLows;
    }
    if(lows < -100) {
        lows = 0;
    } else if(lows > 0) {
        lows = 127;
    } else {
        lows = lows.map(-100, 0, 0, 128);
        lows = Math.floor(lows);
    }
    jsonValueLeft = lows;
    degree_low = lows.map(0,128,-140,140);
    document.getElementById('left_rot3').style.transform = "rotate(" + degree_low + "deg)" ;
}

/**
 * Change the mid parameters and turn corresponding knob in html document
 */
function changeMids() {
    mids = hand.roll() * (180 / Math.PI) * (-1);
    if(Math.abs(oldMids - mids) < knobSensitivity) {
        oldMids = mids;
    } else {
        mids = oldMids;
    }
    if(mids < -100) {
        mids = 0;
    } else if(mids > 0) {
        mids = 127;
    } else {
        mids = mids.map(-100, 0, 0, 128);
        mids = Math.floor(mids);
    }
    jsonValueLeft = mids;
    degree_mid = mids.map(0,128,-140,140);
    document.getElementById('left_rot2').style.transform = "rotate(" + degree_mid + "deg)" ;
}

/**
 * Change the high parameters and turn corresponding knob in html document
 */
function changeHighs() {
    highs = hand.roll() * (180 / Math.PI) * (-1);
    if(Math.abs(oldHighs - highs) < knobSensitivity) {
        oldHighs = highs;
    } else {
        highs = oldHighs;
    }
    if(highs < -100) {
        highs = 0;
    } else if(highs > 0) {
        highs = 127;
    } else {
        highs = highs.map(-100, 0, 0, 128);
        highs = Math.floor(highs);
    }
    jsonValueLeft = highs;
    degree_high = highs.map(0,128,-140,140);
    document.getElementById('left_rot1').style.transform = "rotate(" + degree_high + "deg)" ;
}

/**
 * Change the right bottom knob parameters and turn corresponding knob in html document
 */
function changeRightBot() {
    rightBot = hand.roll() * (180 / Math.PI) * (-1);
    if(Math.abs(oldRightBot - rightBot) < knobSensitivity) {
        oldRightBot = rightBot;
    } else {
        rightBot = oldRightBot;
    }
    if(rightBot < 0) {
        rightBot = 0;
    } else if(rightBot > 100) {
        rightBot = 127;
    } else {
        rightBot = rightBot.map(0, 100, 0, 128);
        rightBot = Math.floor(rightBot);
    }
    jsonValueRight = rightBot;
    degree_rightBot = rightBot.map(0,128,-140,140);
    document.getElementById('right_rot3').style.transform = "rotate(" + degree_rightBot + "deg)" ;
}

/**
 * Change the right middle knob parameters and turn corresponding knob in html document
 */
function changeRightMid() {
    rightMid = hand.roll() * (180 / Math.PI) * (-1);
    if(Math.abs(oldRightMid - rightMid) < knobSensitivity) {
        oldRightMid = rightMid;
    } else {
        rightMid = oldRightMid;
    }
    if(rightMid < 0) {
        rightMid = 0;
    } else if(rightMid > 100) {
        rightMid = 127;
    } else {
        rightMid = rightMid.map(0, 100, 0, 128);
        rightMid = Math.floor(rightMid);
    }
    jsonValueRight = rightMid;
    degree_rightMid = rightMid.map(0,128,-140,140);
    document.getElementById('right_rot2').style.transform = "rotate(" + degree_rightMid + "deg)" ;
}

/**
 * Change the right top knob parameters and turn corresponding knob in html document
 */
function changeRightTop() {
    rightTop = hand.roll() * (180 / Math.PI) * (-1);
    if(Math.abs(oldRightTop - rightTop) < knobSensitivity) {
        oldRightTop = rightTop;
    } else {
        rightTop = oldRightTop;
    }
    if(rightTop < 0) {
        rightTop = 0;
    } else if(rightTop > 100) {
        rightTop = 127;
    } else {
        rightTop = rightTop.map(0, 100, 0, 128);
        rightTop = Math.floor(rightTop);
    }
    jsonValueRight = rightTop;
    degree_rightTop = rightTop.map(0,128,-140,140);
    document.getElementById('right_rot1').style.transform = "rotate(" + degree_rightTop + "deg)" ;
}


/**
 * Change the left fader value and move corresponig fader in html document
 */
function changeLeftHandFlat() {
    if(hand.palmPosition[1] > 100 && hand.palmPosition[1] < 400)
    {
        leftHandFlatEffect = hand.palmPosition[1];
        leftHandFlatEffect = leftHandFlatEffect.map(100,400,0,128);
        leftHandFlatEffect = Math.floor(leftHandFlatEffect);
    } else if (hand.palmPosition[1] <= 100) {
        leftHandFlatEffect = 0;
    } else if (hand.palmPosition[1] >= 400) {
        leftHandFlatEffect = 127;
    }
    jsonValueLeft = leftHandFlatEffect;
}

/**
 * Change the right fader value and move corresponig fader in html document
 */
function changeRightHandFlat() {
    if(hand.palmPosition[1] > 100 && hand.palmPosition[1] < 400)
    {
        rightHandFlatEffect = hand.palmPosition[1];
        rightHandFlatEffect = rightHandFlatEffect.map(100,400,0,24);
        rightHandFlatEffect = Math.floor(rightHandFlatEffect);
    } else if (hand.palmPosition[1] <= 100) {
        rightHandFlatEffect = 0;
    } else if (hand.palmPosition[1] >= 400) {
        rightHandFlatEffect = 24;
    }
    jsonValueRight = rightHandFlatEffect;
}

/**
 * Check if pinch gesture is present
 * @returns {boolean}
 */
function isPinched() {
  if(hand.pinchStrength > 0.75) // && hand.grabStrength < 0.95)
  {
      return true;
  }
  else
  {
      return false;
  }

}

/**
 * Check if grab gesture is present
 * @returns {boolean}
 */
function isGrabbed()
{
    if(hand.grabStrength >= 0.95 && !isPinched())
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * Check if hand flat gesture is present
 * @returns {boolean}
 */
function isFlat() {
    if(!isGrabbed() && !isPinched()) {
      if(Leap.vec3.distance(hand.indexFinger.tipPosition, hand.middleFinger.tipPosition) <= 25 && Leap.vec3.distance(hand.middleFinger.tipPosition, hand.ringFinger.tipPosition) <= 25) {
          return true;
      }
    }
    return false;
}

/**
 * Change knob settings of left panel when left gesture is pinched
 */
function changeFrequency()
{
  if(0 <= hand.palmPosition[1] && hand.palmPosition[1] < 180) {
      changeLows();
      jsonSelectorLeft = "lows";
  } else if(200 <= hand.palmPosition[1] && hand.palmPosition[1] < 320) {
      changeMids();
      jsonSelectorLeft = "mids";
  } else if(340 <= hand.palmPosition[1] && hand.palmPosition[1] < 500) {
      changeHighs();
      jsonSelectorLeft = "highs";
  }
}

/**
 * Change knob settings of right panel when right gesture is pinched
 */
function changeRightHandPinched()
{
  if(0 <= hand.palmPosition[1] && hand.palmPosition[1] < 180) {
      changeRightBot();
      jsonSelectorRight = "bot";
  } else if(200 <= hand.palmPosition[1] && hand.palmPosition[1] < 320) {
      changeRightMid();
      jsonSelectorRight = "mid";
  } else if(340 <= hand.palmPosition[1] && hand.palmPosition[1] < 500) {
      changeRightTop();
      jsonSelectorRight = "top";
  }
}

/**
 * Change fader settings of left panel when left gesture is flat
 */
function changeVolume()
{
  if(hand.palmPosition[1] > 100 && hand.palmPosition[1] < 400)
  {
      volume = hand.palmPosition[1];
      volume = volume.map(100,400,0,128);
      volume = Math.floor(volume);
  } else if (hand.palmPosition[1] <= 100) {
      volume = 0;
  } else if (hand.palmPosition[1] >= 400) {
      volume = 127;
  }
  jsonValueLeft = volume;
}

/**
 * Change fader settings of right panel when right gesture is grabbed
 */
function changeRightHandGrabbed()
{
  if(hand.palmPosition[1] > 100 && hand.palmPosition[1] < 400)
  {
      RightHandGrab = hand.palmPosition[1];
      RightHandGrab = RightHandGrab.map(100,400,0,128);
      RightHandGrab = Math.floor(RightHandGrab);
  } else if (hand.palmPosition[1] <= 100) {
      RightHandGrab = 0;
  } else if (hand.palmPosition[1] >= 400) {
      RightHandGrab = 127;
  }
  jsonValueRight = RightHandGrab;
}

var faderValueLeft = 0;
var faderValueRight = 0;

/**
 * Normalize input so the faders and knobs are in range
 * @param value
 * @returns {*}
 */
function normalizeInput(value) {
    if(value < 40) {
        value = 40;
    } else if(value > 500) {
        value = 500;
    }
    return value;
}

var counter = 0;

/**
 * Send data over socket to server
 */
function emitData()
{
  socket.emit('midi',
    {
      "leftHand":
      {
        "gesture": jsonGestureLeft,
        "selector": jsonSelectorLeft,
        "value": jsonValueLeft
      },
      "rightHand":
      {
        "gesture": jsonGestureRight,
        "selector": jsonSelectorRight,
        "value": jsonValueRight
      }
    });

    console.log(jsonGestureLeft);

}

/**
 * Main program cycle
 */
Leap.loop(opts, function(frame){
  if(counter != 2)
  {
    counter++;
    return;
  }


  counter = 0;
    // frameString = concatData("num_hands", frame.hands.length);
    for(var i = 0 , len = frame.hands.length; i < len; i++ ) {
         hand = frame.hands[i];
        if(hand.type === 'left')
        {
            if(isPinched())
            {
                jsonGestureLeft = "pinch";
                changeFrequency();
                document.getElementById('pinch_left').style.backgroundColor="#e538b6";
            } else {
                document.getElementById('pinch_left').style.backgroundColor="#603493";
            }
            if(isGrabbed())
            {
                jsonGestureLeft = "grab";
                changeVolume();
                jsonSelectorLeft = "";
                document.getElementById('grab_left').style.backgroundColor="#e538b6";
            } else {
                document.getElementById('grab_left').style.backgroundColor="#603493";
            }
            if(isFlat()){
                jsonGestureLeft = "flat";
                changeLeftHandFlat();
                jsonSelectorLeft = "";
                document.getElementById('flat_left').style.backgroundColor="#e538b6";
            } else {
                document.getElementById('flat_left').style.backgroundColor="#603493";
            }
            faderValueLeft = normalizeInput(hand.palmPosition[1]);

            if(faderValueLeft > 0 && faderValueLeft < 160) {
                document.getElementById('left_indicator').style.backgroundColor = "#c44700";
            } else if(faderValueLeft > 180 && faderValueLeft < 320) {
                document.getElementById('left_indicator').style.backgroundColor = "#8a004f";
            } else if(faderValueLeft > 340 && faderValueLeft < 500) {
                document.getElementById('left_indicator').style.backgroundColor = "#2dc2d9";
            }
            faderValueLeft = faderValueLeft.map(40, 500, 0, 100);
            document.getElementById('left_indicator').style.height = "" + faderValueLeft + "%";
            emitData();
        }
        if(hand.type === 'right')
        {
            if(isPinched())
            {
                jsonGestureRight = "pinch";
                changeRightHandPinched();
                document.getElementById('pinch_right').style.backgroundColor="#e538b6";
            } else {
                document.getElementById('pinch_right').style.backgroundColor="#603493";
            }
            if(isGrabbed())
            {
                jsonGestureRight = "grab";
                changeRightHandGrabbed();
                jsonSelectorRight = "";
                document.getElementById('grab_right').style.backgroundColor="#e538b6";
            } else {
                document.getElementById('grab_right').style.backgroundColor="#603493";
            }
            if(isFlat()){
              jsonGestureRight = "flat";
              changeRightHandFlat();
              jsonSelectorRight = "";
                document.getElementById('flat_right').style.backgroundColor="#e538b6";
            } else {
                document.getElementById('flat_right').style.backgroundColor="#603493";
            }

            faderValueRight = normalizeInput(hand.palmPosition[1]);

            if(faderValueRight > 0 && faderValueRight < 160) {
                document.getElementById('right_indicator').style.backgroundColor = "#c44700";
            } else if(faderValueRight > 180 && faderValueRight < 320) {
                document.getElementById('right_indicator').style.backgroundColor = "#8a004f";
            } else if(faderValueRight > 340 && faderValueRight < 500) {
                document.getElementById('right_indicator').style.backgroundColor = "#2dc2d9";
            }
            faderValueRight = faderValueRight.map(40, 500, 0, 100);
            document.getElementById('right_indicator').style.height = "" + faderValueRight + "%";
            emitData();
        }

    }


});
