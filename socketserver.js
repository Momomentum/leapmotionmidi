var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var easymidi = require('easymidi');
var EventEmitter = require('events').EventEmitter;

var emitter = new EventEmitter();
var oldLeftHand = null;
var oldRightHand = null;
var oldNote = 0;
var isPlayed = false;



app.use(express.static('public'));

/**
 * Serves index.html
 */
app.get('/', function(req, res){
    res.sendFile(__dirname + '/public/index.html');
});

/**
 * On connection handle incoming json data
 */
io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('midi', function(data) {
        handleMessage(data);
    });
});

/**
 * Starts the http server
 */
server.listen(3000, function(){
    console.log('listening on *:3000');
});


var output = new easymidi.Output('Virtual_Midi', true);

/**
 * The actual json message handling
 * @param data
 */
function handleMessage(data) {
    // console.log(data);
    if(isValidHandObject(data.leftHand) && isNewHandData(data.leftHand, oldLeftHand)) {
        console.log('new valid data');
        emitter.emit('leftHand', data.leftHand.gesture, data.leftHand.selector, data.leftHand.value);
        oldLeftHand = data.leftHand;

    }
    if(isValidHandObject(data.rightHand) && isNewHandData(data.rightHand, oldRightHand)) {
        console.log('new valid data');
        emitter.emit('rightHand', data.rightHand.gesture, data.rightHand.selector, data.rightHand.value);
        oldRightHand = data.rightHand;
    }
}


/**
 * When data signifies data corresponding to left hand determine which gesture and call function
 */
emitter.on('leftHand', function(gesture,selector,value){
    console.log('left');
    switch(gesture) {
        case 'pinch':
            changePinchLeft(selector, value);
            break;
        case 'grab':
            changeGrabLeft(value);
            break;
        case 'flat':
            changeFlatLeft(value);
            break;
        case 'reset':
            resetMidi();
            break;
        default:
            break;
    }
});

/**
 * When data signifies data corresponding to right hand determine which gesture and call function
 */
emitter.on('rightHand', function(gesture,selector,value){
    console.log('right');
    switch(gesture) {
        case 'pinch':
            changePinchRight(selector, value);
            break;
        case 'grab':
            changeGrabRight();
            break;
        case 'flat':
            changeFlatRight(value);
            break;
        case 'reset':
            resetMidi();
            break;
        default:
            break;
    }
});

/**
 * Create control change midi message for pinch left
 * @param selector
 * @param value
 */
function changePinchLeft(selector, value) {
    switch(selector) {
        case 'highs':
            console.log('send hi cc');
            output.send('cc', {
                controller: 70,
                value: value,
                channel: 1
            });
            break;
        case 'mids':
            console.log('send mid cc');
            output.send('cc', {
                controller: 71,
                value: value,
                channel: 1
            });
            break;
        case 'lows':
            console.log('send low cc');
            output.send('cc', {
                controller: 72,
                value: value,
                channel: 1
            });
            break;
        default:
            break;
    }
}

/**
 * Create control change message for left flat gesture
 * @param value
 */
function changeFlatLeft(value) {
    output.send('cc', {
        controller: 80,
        value: value,
        channel: 1
    });
}

/**
 * Create control change message for left grab gesture
 * @param value
 */
function changeGrabLeft(value) {
    output.send('cc', {
        controller: 7,
        value: value,
        channel: 1
    });
}

/**
 * Create control change message for right pinch gesture
 * @param selector
 * @param value
 */
function changePinchRight(selector, value) {
    switch(selector) {
        case 'top':
            output.send('cc', {
                controller: 73,
                value: value,
                channel: 1
            });
            break;
        case 'mid':
            output.send('cc', {
                controller: 74,
                value: value,
                channel: 1
            });
            break;
        case 'bot':
            output.send('cc', {
                controller: 75,
                value: value,
                channel: 1
            });
            break;
        default:
            break;
    }
}

/**
 * Check if new note is present
 * @param value
 */
function isNewNote(value) {
    if(value == oldNote && isPlayed) {
        return false;
    } else {
        return true;
    }
}

/**
 * Create noteon message for right flat gesture
 * @param value
 */
function changeFlatRight(value) {
    if(isNewNote(value)) {
        oldNote = value;
        isPlayed = true;
        output.send('noteon', {
            note: 48,
            velocity: 120,
            channel: 1
        });
        output.send('cc', {
            controller: 100,
            value: value,
            channel: 1
        });
    }
}

/**
 * Create noteoff message for right flat gesture
 */
function changeGrabRight() {
    console.log('Grab right');
    isPlayed = false;
    output.send('noteon', {
        note: 48,
        velocity: 0,
        channel: 1
    });
}

/**
 * Helper function to determine if hand object corresponds to defined format
 * @param hand
 * @returns {boolean}
 */
function isValidHandObject(hand) {
    if(hand.gesture != '' && hand.value != '') {
        return true;
    } else {
        return false;
    }
}

/**
 * Check if incoming json contains new information
 *
 * @param newHand
 * @param oldHand
 * @returns {boolean}
 */
function isNewHandData(newHand, oldHand) {
    if(JSON.stringify(newHand) === JSON.stringify(oldHand)) {
        return false;
    } else {
        return true;
    }
}


